<?php
require_once 'autoloader.php';

$ormTask = new Task();
$sortBy = '';

if (isPost()) {
    if (!empty($_POST['save'])) {
        $ormTask->add($_POST['description']);
    }

    if (!empty($_POST['edit'])) {
        $ormTask->editData($_GET['id'], $_POST['description']);
        header('Location: index.php');
    }

    if (!empty($_POST['sort'])) {
        $sortBy = $_POST['sort_by'];
    }
}

if (!empty($_GET['id']) && !empty($_GET['action'])) {
    switch ($_GET['action']) {
        case 'done':
        case 'delete':
            $ormTask->{$_GET['action']}($_GET['id']);
            header('Location: index.php');
            break;
        case 'edit':
            $edit = $ormTask->{$_GET['action']}($_GET['id']);
            break;
    }
}

function isPost()
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        return true;
    } else {
        return false;
    }
}

?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset=utf-8">
        <title>Задачи</title>

        <style>
            form {
                margin-bottom: 1em;
            }

            form {
                display: block;
                margin-top: 0em;
            }
            table {
                border-spacing: 0;
                border-collapse: collapse;
            }

            table td, table th {
                border: 1px solid #ccc;
                padding: 5px;
            }

            table th {
                background: #eee;
            }
        </style>

    </head>
    <body>
        <h1>Список дел на сегодня</h1>
        <div style="float: left">
            <form method="POST">
                <input type="text" name="description" placeholder="Описание задачи" value="<?php echo ($edit ? $edit : '') ?>" />
                <input type="submit" name="<?php echo ($edit ? 'edit' : 'save') ?>" value="<?php echo ($edit ? 'Редактировать' : 'Сохранить') ?>" />
            </form>
        </div>
        <div style="float: left; margin-left: 20px;">
            <form method="POST">
                <label for="sort">Сортировать по:</label>
                <select name="sort_by">
                    <option value="date_added">Дате добавления</option>
                    <option value="is_done">Статусу</option>
                    <option value="description">Описанию</option>
                </select>
                <input type="submit" name="sort" value="Отсортировать" />
            </form>
        </div>
        <div style="clear: both"></div>

        <table>
            <tr>
                <th>Описание задачи</th>
                <th>Дата добавления</th>
                <th>Статус</th>
                <th>Действия</th>
            </tr>
            <?php if (!empty($tasks = $ormTask->getAll($sortBy))):
            foreach ($tasks as $task): ?>
            <tr>
                <td><?php echo $task['description'];?></td>
                <td><?php echo $task['date_added'];?></td>
                <?php if ($task['is_done'] == 1){ ?>
                <td><span style='color: green;'>Выполнено</span></td>
                <?php }else{?>
                <td><span style='color: orange;'>В процессе</span></td>
                <?php }?>
                <td>
                    <a href='?id=<?php echo $task['id'];?>&action=edit'>Изменить</a>
                    <a href='?id=<?php echo $task['id'];?>&action=done'>Выполнить</a>
                    <a href='?id=<?php echo $task['id'];?>&action=delete'>Удалить</a>
                </td>
            </tr>
            <?php endforeach;endif;?>
        </table>
    </body>
</html>