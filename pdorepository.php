<?php

class PDORepository{
    const USERNAME="root";
    const PASSWORD="root";
    const HOST="localhost";
    const DB="todo";

    private function getConnection()
    {
        $username = self::USERNAME;
        $password = self::PASSWORD;
        $host = self::HOST;
        $db = self::DB;
        $connection = new PDO("mysql:dbname=$db;host=$host;charset=utf8", $username, $password);
        return $connection;
    }

    public function execSql($sql, $args = [])
    {
        $connection = $this->getConnection();
        $stmt = $connection->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }
    public function getData($sql, $args = [])
    {
        $stmt = self::execSql($sql, $args);
        $data = array();
        while ($row = $stmt->fetch()) {
            $data[] = $row;
        }
        return $data;
    }

    public function insertData($sql, $args = [])
    {
        return self::execSql($sql, $args);
    }

    public function updateData($sql, $args = [])
    {
        return self::execSql($sql, $args);
    }

    public function deleteData($sql, $args = [])
    {
        return self::execSql($sql, $args);
    }
}

?>