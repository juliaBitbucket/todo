<?php
class Task {

    private $db;
    private $table = 'tasks';

    public function __construct()
    {
        $this->db = new PDORepository();
    }

    public function getAll($sortBy = '')
    {
        $sql = "SELECT * FROM $this->table";
        if(!empty($sortBy)) {
            $sql .= " ORDER BY $sortBy";
        }
        return $this->db->getData($sql);
    }

    public function add($description)
    {
        $description = strip_tags($description);
        if(empty($description)) return false;

        $toDay = date('Y-m-d H:i:s');
        $sql = "INSERT INTO $this->table (`description`,`date_added`) VALUES (:description,:toDay)";

        return $this->db->insertData($sql, ['description' => $description, 'toDay' => $toDay]);
    }

    public function done($id)
    {
        $sql = "UPDATE $this->table SET is_done = 1 WHERE id = $id";

        return $this->db->updateData($sql);
    }

    public function delete($id)
    {
        $sql = "DELETE from $this->table WHERE id = $id";

        return $this->db->deleteData($sql);
    }

    public function edit($id)
    {
        $id = (int)$id;
        if (empty($id)) return;
        $task = self::getTaskId($id);
        if(!empty($task[0]['description'])) {
            return $task[0]['description'];
        }
    }

    public function getTaskId($id)
    {
        return $this->db->getData("SELECT * FROM $this->table WHERE id = $id");
    }

    public function editData($id, $description)
    {
        $id = (int)$id;
        if (empty($id)) return false;

        $description = strip_tags($description);
        if(empty($description)) return false;

        $sql = "UPDATE $this->table SET description = '$description' WHERE id = $id";

        return $this->db->updateData($sql);
    }
}